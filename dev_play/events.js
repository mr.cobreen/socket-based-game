const EventEmitter = require('events')

// const emitter = new EventEmitter()

// emitter.on('anything', data => {
//     console.log("anything", data)
// })

// emitter.emit('anything', {a: 1})

class Dispatcher extends EventEmitter {
    subscribe (eventName, cb) {
        console.log("[Subscribe...]")
        this.on(eventName, cb)
    }
    dispatch (eventName, data) {
       console.log("[Dispatch...]")
       this.emit(eventName, data)
    }
}