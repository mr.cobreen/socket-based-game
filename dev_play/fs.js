const fs = require('fs')
const path = require('path')

// fs.mkdir(
//     path.join(__dirname, "superDir"),
//     (err) => {
//         if (err) {
//             throw err
//         }

//         console.log("folder created")
//     }
// )
///////////////////////////////////////////////////
const filePath = path.join(__dirname, 'text.txt')

//also appendFile
// fs.writeFile(filePath, "hello world!", (err) => {
//     if (err) {
//         throw err
//     }
//     console.log('file created')
// })
///////////////////////////////////////////////////
fs.readFile(filePath, (err, content) => {
    if (err) {
        throw err
    }

    console.log("content: " + content)
})