const http = require('http')
const chalk = require('chalk');

const server = http.createServer((req, res) => {
    console.log("new request")

    res.writeHead(200, { 
        'Content-Type': 'text/html'
    })
    res.end("<h1>Hello node.js</h1>")
})

server.listen(3000, () => {
    console.log(chalk.green('Server has been started'))
});