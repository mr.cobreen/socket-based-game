import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueSocketIO from 'vue-socket.io'

window._ = require('lodash');

// require('@mdi/font/scss/materialdesignicons.scss')

Vue.config.productionTip = false

Vue.use(new VueSocketIO({
  debug: false,
  connection: 'http://localhost:2000',
  vuex: {
      store,
      actionPrefix: 'SOCKET_',
      mutationPrefix: 'SOCKET_'
  },
  // options: { path: "/my-app/" } //Optional options
}))

Vue.mixin({
  methods: {
    getRand(min, max) {
      return Math.random() * (max - min) + min;
    },
    loopNum(val, min, max) {
      let p = max-min;
      let mod = (val-min)%p;
      if(mod<0)
        mod += p;
      return min+mod;
    },
    dtr (degrees) {
      return degrees * (Math.PI/180);
    }
  }
});

window.system = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
