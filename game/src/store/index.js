import Vue from 'vue'
import Vuex from 'vuex'
import informaion from './modules/informaion'
import ship from './modules/ship'
import auth from './modules/auth'
import server from './modules/server'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    socket: null
  },
  mutations: {
    set_socket: (state, socket) => {
      state.socket = socket
    }
  },
  actions: {
    // "SOCKET_any"(commit, msg) {
    //   console.log(msg)
    //     // do something
    // }
  },
  getters: {
    get_socket: (state) => {
      return state.socket
    }
  },
  modules: {
    informaion,
    ship,
    auth,
    server
  },
})
