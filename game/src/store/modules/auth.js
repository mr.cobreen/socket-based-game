export default {
    state: {
        user: {
            loggedIn: false,
            username: ""
        }
    },
    mutations: {
        logUserIn: (state, payload) => {
            state.user.loggedIn = true
            state.user.username = payload.username
        },
        SOCKET_SelfInit: (state, payload) => {
            state.user.username = payload.player.username
            state.user.loggedIn = true
        }
    },
    actions: {
        login: async ({commit, getters}, payload) => {
            getters.get_socket.emit("signIn", {
                username: payload.login,
                password: payload.password
            })
        },
        register: async ({commit, getters}, payload) => {
            getters.get_socket.emit("signUp", {
                username: payload.login,
                password: payload.password
            })
        }
    },
    getters: {
        isLoggedIn: (state) => {
            return state.user.loggedIn
        }
    }
}