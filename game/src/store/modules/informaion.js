export default {
    state: {
        currentSnackMessagePlaceholder: {
            on: false,
            timeout: -1,
            bottom: false,
            multiLine: false,
            color: "info",
            str: "",
            id: null
        },
        currentSnackMessage: {
            on: false,
            timeout: -1,
            bottom: false,
            multiLine: false,
            color: "info",
            str: "",
            id: null
        },
        snackMessagesStack: []
    },
    mutations: {
        nextSnackMessage: (state) => {
            const msg = state.snackMessagesStack.shift()
            const placeholderCopy = Object.assign({}, state.currentSnackMessagePlaceholder);
            state.currentSnackMessage = Object.assign(placeholderCopy, msg || {})
        },
        enqueueSnackMessage: (state, payload) => {
            state.snackMessagesStack.push(payload)
        }
    },
    actions: {
        enqueueSnackMessage: async ({commit, state}, payload) => {
            if (!(payload.id && state.currentSnackMessage.id == payload.id) && !state.snackMessagesStack.find(message => {
                return payload.id && message.id === payload.id
            })) {
                console.log("enqueuing")
                commit("enqueueSnackMessage", Object.assign(payload, {on: true}))
                if (!state.currentSnackMessage.on) {
                    commit("nextSnackMessage")
                }
            }
        }
    },
    getters: {
        get_snack_message: (state) => {
            return state.currentSnackMessage
        }
    }
}