export default {
    state: {
        frequency: 40,
    },
    mutations: {
        SOCKET_SelfInit: (state, payload) => {
            state.frequency = payload.player.tickFrequency;
        }
    },
    getters: {
        get_server_tick_frequency(state) {
            return state.frequency;
        }
    }
}