export default {
    state: {
        reactor: {
            on: false,
            onTicker: null,
            charge: 75
        },
        radio: {
            emissionLevel: 0,
            emissionStorage: [],
            direction: 0,
            archRadius: 0,
        },
        navigation: {
            x: 0,
            y: 0,
            v: {
                x: 0,
                y: 0
            },
            course: 0,
            courseV: 0,
        },
        engine: {
            on: false,
            throttle: 0,
        },
        RCS: {
            tl: false,
            t: false,
            tr: false,
            l: false,
            r: false,
            bl: false,
            b: false,
            br: false,
        },
        receivedSignals: [
            /*{
                direction: 0,
            }*/
        ],
    },
    mutations: {
        set_charge: (state, payload) => {
            console.log("charge setted", payload)
            state.reactor.charge = payload
        },
        set_reactor_mode: (state, payload) => {
            console.log("reactor mode", payload ? "on" : "off")
            state.reactor.on = payload
        },
        set_velocity: (state, payload) => {
            state.navigation.v = payload
        },
        SOCKET_SelfInit: (state, payload) => {
            state.navigation.x = payload.player.p.x
            state.navigation.y = payload.player.p.y
            state.navigation.v.x = payload.player.v.x
            state.navigation.v.y = payload.player.v.y
            state.navigation.course = payload.player.course
            state.navigation.courseV = payload.player.courseV
        },
        SOCKET_SelfUpdate: (state, payload) => {
            state.navigation.x = payload.p.x
            state.navigation.y = payload.p.y
            state.navigation.v.x = payload.v.x
            state.navigation.v.y = payload.v.y
            state.navigation.course = payload.course
            state.navigation.courseV = payload.courseV
            state.radio.emissionStorage.push(state.radio.emissionLevel)
            if (state.radio.emissionStorage.length > 1000) {
                state.radio.emissionStorage.splice(0, 400)
            }
            state.radio.emissionLevel = payload.emissionLevel
        }
    },
    actions: {
        turn_reactor_on: ({commit, state}) => {
            commit("set_reactor_mode", true)
        },
        turn_reactor_off: ({commit, state}) => {
            commit("set_reactor_mode", false)
        }, 
    },
    getters: {
        get_charge: (state) => {
            return state.reactor.charge
        },
        reactoryIsOn: (state) => {
            return state.reactor.on
        },
        navigation: (state) => {
            return state.navigation
        },
        engine: (state) => {
            return state.engine
        },
        radio: (state) => {
            return state.radio
        }
    }
}
