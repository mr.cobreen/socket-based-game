const host = 'localhost'
const port = 8085

module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  devServer: {
    host: 'localhost'
  }
}