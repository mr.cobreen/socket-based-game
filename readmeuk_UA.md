# Встановлення, та використання

## Сервер

Усі дії слід виконувати в папці `server`

### Підготовка

1. Встанови, та налаштуй mongoDB
2. `npm install`
3. В `Database.js` налаштуй підключення до mongodb
`MongoClient.connect('Стрічка підключення сюди!'`

### Запуск

В папці server запусти `npm run dev`

## Клієнт

Усі дії слід виконувати в папці `game`

### Підготовка

1. `npm install`

### Запуск

Запусти сервер командою `npm run serve`

# Корисне

1. Візуальна частина зроблена за допомогою: [Vuetify](https://vuetifyjs.com/en/introduction/why-vuetify/)
2. Зв'язок з сервером було зроблено за допомогою: [Socket.io](https://socket.io/)
3. Клієнт:  [Vue.js](https://vuejs.org/)
4. Сервер: [Node.js](https://nodejs.org/uk/)
5. База данних: [MongoDB](https://www.mongodb.com)

# Рекомендації

Для комфортнішого використання mongodb рекомендується завантажити [переглядач](https://www.mongodb.com/products/compass)

Всі найдені помилки слід записувати в issues на репозиторії, щоб я міг згодом їх виправити не шукаючи.