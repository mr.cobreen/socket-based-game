var bcrypt = require('bcrypt');

cryptPassword = (password) => {
	return new Promise((resolve, reject) => {
		bcrypt.genSalt(10, function(err, salt) {
			if (err) 
				resolve(err);
			
			bcrypt.hash(password, salt, function(err, hash) {
				resolve(hash);
			});
		});
	})
}; 

comparePassword = (plainPass, hashword) => {
	return new Promise((resolve, reject) => {
		bcrypt.compare(plainPass, hashword, function(err, isPasswordMatch) {   
			return err == null ?
				resolve(isPasswordMatch) :
				resolve(false);
		});
	})
};

USE_DB = true;
const MongoClient = USE_DB ? require('mongodb').MongoClient : null;
global.db = null;
if (USE_DB) {
	MongoClient.connect('mongodb://127.0.0.1:27017/?gssapiServiceName=mongodb', (err, client) => {
		if (err) throw err;
		db = client.db("game")
		emitter.emit('db_ready')
	});
} 

//account:  {username:string, password:string}
//progress:  {username:string, items:[{id:string,amount:number}]}
DB = {};
require('./models/PlayerModel.js')