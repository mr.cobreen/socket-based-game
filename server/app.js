const EventEmitter = require('events');
class Emitter extends EventEmitter {}
emitter = new Emitter()

chalk = require('chalk');
require('./Database');
require('./data_structures/index')
require('./providers/index')
// require('./client/Inventory');

const options = { /* ... */ };
const PORT = 2000
tickFrequency = 40
global.io = require('socket.io')(options);

io.on('connection', socket => { /* ... */ });

io.listen(PORT);
console.log(`Server started at port ${PORT}.`);

global.SOCKET_LIST = {};


var DEBUG = true;

io.on('msg', function(socket, data) {
	console.log('got msg');
})

io.sockets.on('connection', function(socket) {
	console.log('New client :D')
	SOCKET_LIST[socket.id] = socket;
	global.socket = socket;
	const controller = require('./logic/auth_controller')

    socket.on('signIn', controller.signIn);
    socket.on('signUp', controller.signUp);
    socket.on('whoAmI', controller.signUp);

    socket.on('disconnect',function(){
        console.log('Lost client :(')
        delete SOCKET_LIST[socket.id];
        Player.TurnPassive(socket.id);
    });
    
    socket.on('evalServer',function(data){
        if(!DEBUG)
        return;
        var res = eval(data);
        socket.emit('evalAnswer',res);		
    });
});

const tick = () => {
	console.log(chalk.green(`Cycle start`))
	// console.log(Object.keys(Player.list).length)

	for (const playerId in Player.list) {
		const socket = Player.list[playerId].socket;
		Player.list[playerId].update()
	}
	for (const playerId in Player.list) {
		const socket = Player.list[playerId].socket;
		if (socket) {
			socket.emit('SelfUpdate', Player.list[playerId].getSelfUpdatePack());
			Player.list[playerId].resetReceivedSignal()
		}
		if (!socket) {
			// console.log("UNUSED SOCKET")
		}
	}	
	console.log(chalk.red(`Cycle end`))
}

//SERVER
setInterval(tick, tickFrequency);
//END SERVER

//CONSOLE
process.stdin.on('data', function (text) {
	const command = text.trim().split(' ')
	switch (command[0]) {
		case "emit":
			const msg = command[1].trim().split(':')
			io.emit(msg[0], msg[1]);
			console.log(`Emitted '${msg[1]}' at event '${msg[0]}'`);
		break;
		case "save":
			saveState()
			console.log(`successfully saved all states`);
		break;
		case "save":
			loadState()
			console.log(`successfully loaded all states`);
		break;
		case "quit":
			done();
		break;
	}
});
process.stdin.resume();
process.stdin.setEncoding('utf8');
function done() {
  console.log('Now that process.stdin is paused, there is nothing more to do.');
  process.exit();
}
//END CONSOLE

function saveState () {
	for (const playerId in Player.list) {
		Player.list[playerId].save()
	}
}

async function loadState () {
	const players = await DB.getAllPlayers()
	// console.log(players)
	Player.list = {}
	for( const playerId in players ) {
		Player.initAndLoad(null, players[playerId]);
	}
}

emitter.on('db_ready', async () => {
	await loadState()
});

/*
var profiler = require('v8-profiler');
var fs = require('fs');
var startProfiling = function(duration){
	profiler.startProfiling('1', true);
	setTimeout(function(){
		var profile1 = profiler.stopProfiling('1');
		
		profile1.export(function(error, result) {
			fs.writeFile('./profile.cpuprofile', result);
			profile1.delete();
			console.log("Profile saved.");
		});
	},duration);	
}
startProfiling(10000);
*/