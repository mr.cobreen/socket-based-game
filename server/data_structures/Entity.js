/*every entity is a point with additional:
speed, acceleration
every entity can:
be computed
*/
Entity = function(param){
	var self = Point(param);
	self.v = {
		x: 0,
		y: 0
	}
	if(param){
		if(param.v) {
			if(param.v.x)
				self.v.x = param.v.x;
			if(param.v.y)
				self.v.y = param.v.y;
		}
	}
	self.update = () => {
		let doUpdate = !false;
		if (self.v.x > -0.5 && self.v.x < 0.5 && self.v.x != 0) {
			self.v.x = 0
			doUpdate = true
		}
		if (self.v.y > -0.5 && self.v.y < 0.5 && self.v.y != 0 ) {
			self.v.y = 0
			doUpdate = true
		}
		if (self.v.x != 0 || self.v.y != 0) {
			doUpdate = true
		} else if (self.x != Math.floor(self.x) || self.y != Math.floor(self.y)) {
			self.x = Math.floor(self.x)
			self.y = Math.floor(self.y)
			doUpdate = true
		}
		self.x += self.v.x
		self.y += self.v.y
		return doUpdate
	}

	const SuperGetSelfInitPack = self.getSelfInitPack
	self.getSelfInitPack = () => {
		let res = SuperGetSelfInitPack()
		res.v = self.v
		return res
	}
	self.getSelfUpdatePack = () => {
		return {
			p: {
				x: player.x,
				y: player.y
			},
			v: {
				x: player.v.x,
				y: player.v.y
			},
			course: player.course,
			courseV: player.courseV
		}
	}
	return self;
}