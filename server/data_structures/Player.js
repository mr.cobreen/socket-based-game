//Refresh socket event assignation

const e = require("express");


Player = (param) => {
	var self = Entity(param);

	self.socket = param.socket || null;
	self.username = param.username || null
	self.course = param.course || 0
	self.courseV = param.courseV || 0
	self.emissionReceived = 0

	var timestampStart = Date.now()

	self.input = {
		engine: {
			on: {
				type: "boolean",
				value: false,
			},
			throttle: {
				type: "number",
				value: 0,
			},
		},
		radar: {
			direction: {
				type: "number",
				value: 0,
			},
			archRadius: {
				type: "number",
				value: 0,
			}
		}
	}

	self.getDistinctableId = () => {
		return self.socket && self.socket.id ? self.socket.id : self.username
	}

	self.attachInputEvents = () => {
		self.socket.on("tlClick", () => {		
			const courseInRadians = self.dtr(self.loopNum(self.course-45, 0, 360))
			self.v.y += Number((Math.cos(courseInRadians) * -1).toFixed(3))
			self.v.x += Number((Math.sin(courseInRadians) * 1).toFixed(3))
		})
		self.socket.on("tClick", () => {
			const courseInRadians = self.dtr(self.loopNum(self.course, 0, 360))
			self.v.y += Number((Math.cos(courseInRadians) * -1).toFixed(3))
			self.v.x += Number((Math.sin(courseInRadians) * 1).toFixed(3))
		})
		self.socket.on("trClick", () => {
			const courseInRadians = self.dtr(self.loopNum(self.course+45, 0, 360))
			self.v.y += Number((Math.cos(courseInRadians) * -1).toFixed(3))
			self.v.x += Number((Math.sin(courseInRadians) * 1).toFixed(3))
		})
		self.socket.on("lClick", () => {
			const courseInRadians = self.dtr(self.loopNum(self.course-90, 0, 360))
			self.v.y += Number((Math.cos(courseInRadians) * -1).toFixed(3))
			self.v.x += Number((Math.sin(courseInRadians) * 1).toFixed(3))
		})
		self.socket.on("rClick", () => {
			const courseInRadians = self.dtr(self.loopNum(self.course+90, 0, 360))
			self.v.y += Number((Math.cos(courseInRadians) * -1).toFixed(3))
			self.v.x += Number((Math.sin(courseInRadians) * 1).toFixed(3))
		})
		self.socket.on("blClick", () => {
			const courseInRadians = self.dtr(self.loopNum(self.course-135, 0, 360))
			self.v.y += Number((Math.cos(courseInRadians) * -1).toFixed(3))
			self.v.x += Number((Math.sin(courseInRadians) * 1).toFixed(3))
		})
		self.socket.on("bClick", () => {
			const courseInRadians = self.dtr(self.loopNum(self.course-180, 0, 360))
			self.v.y += Number((Math.cos(courseInRadians) * -1).toFixed(3))
			self.v.x += Number((Math.sin(courseInRadians) * 1).toFixed(3))
		})
		self.socket.on("brClick", () => {
			const courseInRadians = self.dtr(self.loopNum(self.course+135, 0, 360))
			self.v.y += Number((Math.cos(courseInRadians) * -1).toFixed(3))
			self.v.x += Number((Math.sin(courseInRadians) * 1).toFixed(3))
		})
		self.socket.on("rotateLClick", () => {
			self.courseV -= 1
		})
		self.socket.on("rotateRClick", () => {
			self.courseV += 1
		})
		const ParseVariableEvents = (obj, str="") => {
			for (var k in obj)
			{
				if (typeof obj[k] == "object" && obj[k] !== null && obj[k].type === undefined)
				ParseVariableEvents(obj[k], str+k+'.');
				else {
					((str, k, obj) => {
						console.log(`${str}${k}Set`)
						self.socket.on(`${str}${k}Set`, (val) => {
							if (typeof val == obj[k].type) {
								console.log(`${obj[k].value} ${k} SET ${val}`)
								obj[k].value = val
							} else {
								self.socket.emit(
									"ValidationError", 
									`An error occurred while trying to set ${k} to ${val}. Input type missmatch`
								)
							}
						})
					})(str, k, obj)
				}
			}
		}
		ParseVariableEvents(self.input)
	}
	if (self.socket)
		self.attachInputEvents()
	
	const SuperUpdate = self.update
	self.update = () => {
		let doUpdate = SuperUpdate()
		if (self.courseV != 0) {
			doUpdate = true
		}
		// console.log("Engine", self.input.engine.on.value)
		if (self.input.engine.on.value) {
			const courseInRadians = self.dtr(self.loopNum(self.course, 0, 360))
			const AccelerationMultiplier = Math.floor(self.input.engine.throttle.value / 10)
			self.v.y += Number((Math.cos(courseInRadians) * -1 * AccelerationMultiplier).toFixed(3))
			self.v.x += Number((Math.sin(courseInRadians) * 1 * AccelerationMultiplier).toFixed(3))
			doUpdate = true
		}
		self.course += self.courseV
		self.course = self.loopNum(self.course, 0, 360);
		self.produceEmission()
		return doUpdate
	}
	self.computeEmission = () => {
		timestampStart += 0.1
		return Math.sin(timestampStart) * 10
	}
	self.produceEmission = () => {
		const emissionLevel = self.computeEmission()

        for (playerId in Player.list) {
            if (
                Player.list[playerId].getDistinctableId() != self.getDistinctableId()
                &&
                Player.list[playerId].getDistance({x: self.x, y: self.y}) < 50
            ) {
				//get self position on "normal" circle
				const pointOnCircle = Player.list[playerId].getPointAtDistance({x: self.x, y: self.y}, 1)
				//normalize coordinates (max = 1 && min = -1)
				const pointOnCircleNormalized = {
					x: pointOnCircle.x - self.x,
					y: pointOnCircle.y - self.y
				}
				//get self pelleng on remote player (Player.list[playerId] hears self at "X" pelleng)
                const pelleng = Player.list[playerId].loopNum(Player.list[playerId].rtd(Math.atan2(pointOnCircleNormalized.y, pointOnCircleNormalized.x)) + 90, 0, 360)
				
				//Get Player.list[playerId] hearing config
                const dividedArch = Player.list[playerId].input.radar.archRadius.value / 2
                const beginAngle = self.loopNum(Player.list[playerId].input.radar.direction.value - dividedArch, 0, 360)
				const endAngle = self.loopNum(Player.list[playerId].input.radar.direction.value + dividedArch, 0, 360)
				
				console.log(`${Player.list[playerId].username} hears ${self.username} (${dividedArch}) ${beginAngle} - ${pelleng} - ${endAngle}`)

				if (beginAngle <= pelleng && endAngle >= pelleng) {
					Player.list[playerId].receiveEmission(emissionLevel, pelleng)
				} else if (beginAngle <= pelleng && beginAngle > endAngle) {
					Player.list[playerId].receiveEmission(emissionLevel, pelleng)
                } else if (beginAngle < endAngle <= pelleng && endAngle >= pelleng) {
					Player.list[playerId].receiveEmission(emissionLevel, pelleng)
				}
            }
		}
		console.log()
		/*\
		|*|
		\*/
	}
	self.receiveEmission = (e) => {
		self.emissionReceived += e;
		console.log(`${self.username}(${self.x}:${self.y}) received ${e} now ${self.emissionReceived}`);
	}

	self.resetReceivedSignal = () => {
		self.emissionReceived = 0
	}

	const SuperGetSelfInitPack = self.getSelfInitPack
	self.getSelfInitPack = () => {
		let res = SuperGetSelfInitPack()
		res.username = self.username
		res.course = self.course
		res.courseV = self.courseV
		res.emissionLevel = self.emissionLevel
		res.tickFrequency = tickFrequency
		return res
	}

	const SuperGetSelfUpdatePack = self.getSelfUpdatePack
	self.getSelfUpdatePack = () => {
		let res = SuperGetSelfInitPack()
		res.course = self.course
		res.courseV = self.courseV
		res.emissionLevel = self.emissionReceived
		return res
	}

	Player.list[self.socket && self.socket.id ? self.socket : self.username] = self;
	
	self.save = () => {
		DB.savePlayerProgress(self)
	}

	return self;
}
Player.list = {};
Player.initAndLoad = (socket, info) => {
	//make whole new player or set to loaded one
	//is current socket not assigned to some player entity
	const existingPlayerId = Object.keys(Player.list).find((key)=> {
		return Player.list[key].username == info.username
	})
	//is player already in system
	if (existingPlayerId) {
		console.log("player reasignation")
		Player.list[socket.id] = Player.list[existingPlayerId]
		Player.list[socket.id].socket = socket
		Player.list[socket.id].attachInputEvents()
		delete Player.list[existingPlayerId]
	} else {
		console.log("actual user init")
		Player(
			Object.assign(
				{}, 
				info, 
				{
					id: socket && socket.id ? socket.id : null,
					socket: socket || null,
				}
			)
		);
	}
	if (socket) {
		socket.emit('SelfInit',{
			selfId: socket.id,
			player: Player.list[socket.id].getSelfInitPack(),
		})			
	}
}
Player.TurnPassive = (socketID) => {
	let player = Player.list[socketID];
	if(!player)
		return;
	player.save()
	Player.list[Player.list[socketID].username] = Player.list[socketID]
	delete Player.list[socketID]
}
