/*every point must have:
id, position
every point can:
compute distance to the point in space
*/
Point = function(param){
	var self = {
		x:0,
		y:0,
		id:"",
	}
	if(param){
		if (param.p) {
			if(param.p.x)
				self.x = param.p.x;
			if(param.p.y)
				self.y = param.p.y;
		}
		if(param.id)
			self.id = param.id;		
	}

	self.dtr = (degrees) => {
		return degrees * (Math.PI/180);
	}
	self.rtd = (radians) => {
  		return radians * (180/Math.PI);
	}

	self.loopNum = (val, min, max) => {
		let p = max-min;
		let mod = (val-min)%p;
		if(mod<0)
		  mod += p;
		return min+mod;
	}

	self.getDistance = (pt) => {
		// console.log(`${self.x}:${self.y} - ${pt.x}:${pt.y} == ${Math.sqrt(Math.pow(self.x-pt.x,2) + Math.pow(self.y-pt.y,2))}`)
		return Math.sqrt(Math.pow(self.x-pt.x,2) + Math.pow(self.y-pt.y,2));
	}
	self.getPointAtDistance = (pt, preferedDistance) => {
		const d = self.getDistance(pt);
		// console.log(`d:${d}`)
		const t = preferedDistance/d;
		return {
			x: (1 - t) * pt.x + (t * self.x),
			y: (1 - t) * pt.y + (t * self.y),
		}
	}
    
    self.getSelfInitPack = function(){
		return {
			id:self.id,
			p: {
				x: self.x,
				y: self.y,
            },
        }
	}
	return self;
}