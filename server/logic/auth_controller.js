module.exports = {
    async signIn (data) {
        const loginResult = await DB.tryToLogIn(data);
        // console.log({loginResult})
        if (loginResult) {    
            const userData = await DB.getPlayer(data.username)
            Player.initAndLoad(socket, userData);
            return socket.emit('signInResponse',{success:true});
        } else {
            return socket.emit('signInResponse', {success:false});
        }
    },
    async signUp (data) {
        const isUsernameTaken = await DB.isUsernameTaken(data);
        if (!isUsernameTaken) {
            await DB.addUser(data);
        }
        socket.emit('signUpResponse',{success: !isUsernameTaken});
    },
    whoAmI () {

    }
}