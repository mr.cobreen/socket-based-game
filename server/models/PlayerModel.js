DB.tryToLogIn = (data) => {
	return new Promise(async (resolve) => {
		if(!USE_DB)
			return resolve(true);
		db.collection('account').findOne({username:data.username}, async (err,res) => {
			if (res) {
				const passwordCheckResult = await comparePassword(data.password, res.password)
				if(passwordCheckResult)
					resolve(true);
			}
			resolve(false);
		})
	});
}
DB.isUsernameTaken = (data) => {
	return new Promise((resolve) => {
		if(!USE_DB)
	        return resolve(false);
		db.collection('account').findOne({username:data.username}, (err, res) => {
			if(res)
				resolve(true);
			else
				resolve(false);
		});
	})
}
DB.addUser = (data) => {
	return new Promise(async (resolve) => {
		const hashedPassword = await cryptPassword(data.password)
	    if(!USE_DB)
	        return resolve();
		db.collection('account').insert({username: data.username, password: hashedPassword}, (err) => {
			resolve();
        	// DB.savePlayerProgress({username: data.username, items: []}, function(){
        	// })
		});
	});
}
DB.getPlayer = (username) => {
	return new Promise((resolve, reject) => {
		if(!USE_DB)
	        return resolve({items:[]});
		db.collection('account').findOne({username:username},function(err,res){
			resolve(res);
		});
	})
}
DB.getAllPlayers = async() => {
    return await new Promise((resolve, reject) => {
		if(!USE_DB && !db)
	        return resolve([]);
		db.collection('account').find({}).toArray((err, res) => {
			resolve(res);
		});
	})
}
DB.savePlayerProgress = async (player) => {
    if(!USE_DB)
        return;
	db.collection('account').updateOne(
		{username:player.username},
		{
			$set: { 
				p: {
					x: player.x,
					y: player.y,
				},
				v: {
					x: player.v.x,
					y: player.v.y,
				},
				input: {
					engine: {
						on: player.input.engine.on,
						throttle: player.input.engine.throttle,
					}
				}
			}
		}, 
		(err, result) => {
			console.log(`Saved ${player.username}`)
		}
	);
}