EntityProvider = (() => {
    let self = {}
    self.getAvailableRadioListeners = () => {
        return Player.list
    }
    self.getAvailableRadioListenersExcept = (ids) => {
        return ((obj, predicate) => {
            let result = {}, key;
        
            for (key in obj) {
                if (obj.hasOwnProperty(key) && predicate(key, obj[key])) {
                    result[key] = obj[key];
                }
            }
        
            return result;
        })(Player.list, (key, item) => {
            return !ids.find(item => item==key)
        });
        //Player.list.filter((index, item) => !ids.find(item => item==index))
    }
    return self
})()